class MultimediaFiles {
    constructor(public fechaCreacion: string, public fechaModificacion: string, public nombre: string, public tipoElemento: string) {
    }

    desplegarInformacion(): void {
        const informacion: string = this.nombre + " " + this.tipoElemento + " " + this.fechaModificacion + " " + this.fechaCreacion;

        console.log(informacion)
    }
}

class DynamicFile extends MultimediaFiles {
    // duracion: string = "";
    constructor(public fechaCreacion: string, public fechaModificacion: string, public nombre: string, public tipoElemento: string, public duracion:string) {
        super(fechaCreacion, fechaModificacion, nombre, tipoElemento);
    }

    detener(): void {
        console.log('Deteniendo...')
    }

    pausar(): void {
        console.log('Pausando...')
    }

    reproducir(): void {
        console.log('Reproduciendo....')
    }
}

class StaticFile extends MultimediaFiles {
    editar(): void {
        console.log('Editando ....')
    }
}
const mFile = new MultimediaFiles("1","2","3","4");

const dFile = new DynamicFile("1","2","3","4","5");
const sFile = new StaticFile("1","2","3","4");


