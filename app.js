"use strict";
class MultimediaFiles {
    constructor(fechaCreacion, fechaModificacion, nombre, tipoElemento) {
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.nombre = nombre;
        this.tipoElemento = tipoElemento;
    }
    desplegarInformacion() {
        const informacion = this.nombre + " " + this.tipoElemento + " " + this.fechaModificacion + " " + this.fechaCreacion;
        console.log(informacion);
    }
}
class DynamicFile extends MultimediaFiles {
    // duracion: string = "";
    constructor(fechaCreacion, fechaModificacion, nombre, tipoElemento, duracion) {
        super(fechaCreacion, fechaModificacion, nombre, tipoElemento);
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.nombre = nombre;
        this.tipoElemento = tipoElemento;
        this.duracion = duracion;
    }
    detener() {
        console.log('Deteniendo...');
    }
    pausar() {
        console.log('Pausando...');
    }
    reproducir() {
        console.log('Reproduciendo....');
    }
}
class StaticFile extends MultimediaFiles {
    editar() {
        console.log('Editando ....');
    }
}
const mFile = new MultimediaFiles("1", "2", "3", "4");
const dFile = new DynamicFile("1", "2", "3", "4", "5");
const sFile = new StaticFile("1", "2", "3", "4");
